﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ViewingAngle
{
    interface IFovContainer
    {
        void RemoveFov(FieldOfViewVm fov);
    }
}
