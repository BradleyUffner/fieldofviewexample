﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Media;
using AgentOctal.WpfLib;
using AgentOctal.WpfLib.Commands;

namespace ViewingAngle
{
    class MainWindowVm : ViewModel, IFovContainer
    {
        public MainWindowVm()
        {
            Fields = new ObservableCollection<FieldOfViewVm>();
            _rnd = new Random();
            _colors = new List<string>(typeof(Colors).GetProperties().Where(p=>p.PropertyType == typeof(Color)).Select(p=>p.Name));
        }

        private readonly Random _rnd;
        private readonly List<string> _colors;

        private string PickRandomColor()
        {
            return _colors[_rnd.Next(_colors.Count)];
        }

        public IReadOnlyList<string> Colors => _colors;

        public ObservableCollection<FieldOfViewVm> Fields { get; }

        private ICommand _addFovCommand;
        public ICommand AddFovCommand
        {
            get
            {
                return _addFovCommand ?? (_addFovCommand = new SimpleCommand((obj) =>
                {
                    var fov = new FieldOfViewVm(this);
                    fov.Color = PickRandomColor();
                    fov.TargetAngle = _rnd.Next(360);
                    fov.FieldOfView = _rnd.Next(90);
                    Fields.Add(fov);
                }));
            }
        }

        public void RemoveFov(FieldOfViewVm fov)
        {
            Fields.Remove(fov);
        }
    }
}
