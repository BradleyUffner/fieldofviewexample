﻿using System;
using System.Windows;
using System.Windows.Input;
using AgentOctal.WpfLib;
using AgentOctal.WpfLib.Commands;

namespace ViewingAngle
{
    class FieldOfViewVm : ViewModel
    {
        public FieldOfViewVm(IFovContainer container)
        {
            Container = container;

            FieldOfView = 45;
            TargetAngle = 90;
            CenterX = 250;
            CenterY = 250;
            Radius = 250;
        }

        public IFovContainer Container { get; }

        private int _fieldOfView;
        public int FieldOfView
        {
            get { return _fieldOfView; }
            set
            {
                SetValue(ref _fieldOfView, value);
                RecalculateArc();
            }
        }

        private int _targetAngle;
        public int TargetAngle
        {
            get { return _targetAngle; }
            set
            {
                SetValue(ref _targetAngle, value);
                RecalculateArc();
            }
        }

        private int _centerX;
        public int CenterX
        {
            get {return _centerX;}
            set
            {
                SetValue(ref _centerX, value);
                CenterPoint = new Point(_centerX,_centerY);
                RecalculateArc();
            }
        }

        private int _centerY;
        public int CenterY
        {
            get {return _centerY;}
            set
            {
                SetValue(ref _centerY, value);
                CenterPoint = new Point(_centerX, _centerY);
                RecalculateArc();
            }
        }

        private int _radius;
        public int Radius
        {
            get {return _radius;}
            set
            {
                SetValue(ref _radius, value);
                ArcSize = new Size(_radius,_radius);
                RecalculateArc();
            }
        }

        private string _color;
        public string Color
        {
            get {return _color;}
            set {SetValue(ref _color, value);}
        }

        private double GetRadians(int angle)
        {
            return angle * Math.PI / 180;
        }

        private ICommand _removeCommand;
        public ICommand RemoveCommand
        {
            get
            {
                return _removeCommand ?? (_removeCommand = new SimpleCommand((obj) =>
                {
                    Container.RemoveFov(this);
                }));
            }
        }

        private void RecalculateArc()
        {
            var targetAngle = GetRadians(_targetAngle);
            var fieldOfView = GetRadians(_fieldOfView);
            var halfFieldOfView = fieldOfView / 2;

            var startAngle = targetAngle - halfFieldOfView;
            var endAngle = targetAngle + halfFieldOfView;
            double angleDiff = endAngle - startAngle;
            IsLargeArc = angleDiff >= Math.PI;

            StartPoint = new Point(CenterX + Radius * Math.Cos(startAngle), CenterY + Radius * Math.Sin(startAngle));
            EndPoint = new Point(CenterX + Radius * Math.Cos(endAngle), CenterY + Radius * Math.Sin(endAngle));
        }

        private Point _startPoint;
        public Point StartPoint
        {
            get { return _startPoint; }
            set { SetValue(ref _startPoint, value); }
        }

        private Point _endPoint;
        public Point EndPoint
        {
            get { return _endPoint; }
            set { SetValue(ref _endPoint, value); }
        }

        private bool _isLargeArc;
        public bool IsLargeArc
        {
            get { return _isLargeArc; }
            set { SetValue(ref _isLargeArc, value); }
        }

        private Point _centerPoint;
        public Point CenterPoint
        {
            get {return _centerPoint;}
            set {SetValue(ref _centerPoint, value);}
        }

        private Size _arcSize;
        public Size ArcSize
        {
            get {return _arcSize;}
            set {SetValue(ref _arcSize, value);}
        }


    }
}
